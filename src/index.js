import React, {useState} from 'react';
import ReactDOM from 'react-dom';
import './App.css'
import 'bulma/css/bulma.css'

const Header = (props) => {
    return (
        <div className='card-header'>
            <h1 className='card-header-title header'>
                You have {props.numTodos} Todos
            </h1>
        </div>
    );
}

const TodoList = (props) => {
    const todos = props.tasks.map((todo, index) => {
        return <Todo
            content={todo}
            key={index}
            id={index}
            onDelete={props.onDelete}
        />
    });

    return (
        <div className='list-wrapper'>
            {todos}
        </div>
    );
}

const Todo = (props) => {
    return (
        <div className='list-item'>
            {props.content}
            <button
                className="delete is-pulled-right"
                onClick={() => {
                    props.onDelete(props.id)
                }}
            ></button>
        </div>
    );
}

function SubmitForm(props) {

    const [term, setTerm] = useState('');

    const handleSubmit = (e) => {
        e.preventDefault();
        if (term === '') return;
        props.onFormSubmit(term);
        setTerm('');
    }

    return (
        <form onSubmit={handleSubmit}>
            <input
                type='text'
                className='input'
                placeholder='Enter Item'
                value={term}
                onChange={(e) => setTerm(e.target.value)}
            />
            <button className='button'>Submit</button>
        </form>
    );
}


const Settings = React.memo(({toggleColor}) => {
    return <div className='settings-component'>
        <span>Settings:</span>
        <button onClick={toggleColor} className='button'>change background color</button>
    </div>;
});


function Todos() {
    const [tasks, setTasks] = useState(['task 1', 'task 2', 'task 3']);

    const handleSubmit = task => {
        setTasks([...tasks, task]);
    }

    const handleDelete = (index) => {
        const newArr = [...tasks];
        newArr.splice(index, 1);
        setTasks(newArr);
    }

    return (
        <div className='card frame'>
            <Header
                numTodos={tasks.length}
            />
            <TodoList
                tasks={tasks}
                onDelete={handleDelete}
            />
            <SubmitForm onFormSubmit={handleSubmit}/>
        </div>
    )
}


function App() {

    const [isColorDark, setIsColorDark] = useState(false);

    const handleChangeColor = () => {
        setIsColorDark(!isColorDark);
    }

    return (
        <div className={`wrapper ${isColorDark ? 'dark' : 'white'}`}>
            <Settings toggleColor={handleChangeColor}/>
            <Todos/>
        </div>
    );
}

ReactDOM.render(
    <App/>,
    document.querySelector('#root')
);
